package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.ProductRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class ApiProductController {

	@Autowired
	public ProductRepository productRepository;
	
	@Autowired
	public VariantRepository variantRepository;
	
	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct(){
		try {
		List<Product> product = this.productRepository.findProductOrdered();
		return new ResponseEntity<>(product, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("find/variant/{id}") // nyari variant by category id
	public ResponseEntity<List<Variant>> getVariantByCateg(@PathVariable("id") Long id){
		try {
			List<Variant> variant = this.productRepository.findVariantByCategory(id);
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/product") // nambah product
	public ResponseEntity<Object> saveProduct(@RequestBody Product product){
		product.setIsActive(true);
		Product productData = this.productRepository.save(product);
		if(productData.equals(product)) {
			return new ResponseEntity<Object>("Save Data Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("product/{id}") // liat produk by id
	public ResponseEntity<List<Product>> getProductByid(@PathVariable("id") Long id){
		try {
			Optional<Product> product = this.productRepository.findById(id);
			if(product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
			
		} catch (Exception e) {
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}
	}

	
	@PutMapping("edit/product/{id}") // edit produk dengan id ini
	public ResponseEntity<Object> updatedProduct(@RequestBody Product product, @PathVariable("id") Long id){
		product.setIsActive(true);
		Optional<Product> productdata = this.productRepository.findById(id);
		if(productdata.isPresent()) {
			product.setId(id);
			this.productRepository.save(product);
			ResponseEntity rest = new ResponseEntity <>("Update Success", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PutMapping("delete/product/{id}") // delete produk dengan id ini
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") Long id){
		Optional<Product> productdata = this.productRepository.findById(id);
		if (productdata.isPresent()) {
			Product product = new Product();
			product.setId(id);
			this.productRepository.deleteProductById(id);
			ResponseEntity rest = new ResponseEntity<>("Delete Success", HttpStatus.OK);
			return rest;
	} else {
		return ResponseEntity.notFound().build();
	}
}
}
