package com.xsisacademy.bootcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.ProductRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@Controller
@RequestMapping("/product/")

public class ProductController {
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private VariantRepository variantRepositroy;
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("product/index");
		List<Product> listProduct = this.productRepository.findAll();
		view.addObject("listproduct", listProduct);
		return view;
	}
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("product/indexapi");
		return view;
	}
	
	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView view = new ModelAndView("product/addform");
		Product product = new Product();
		view.addObject("product", product);
		
		List<Variant> listVariant = this.variantRepositroy.findAll();
		view.addObject("listvariant", listVariant);
		return view;
	}
	
	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Product product, BindingResult result) {
		if(!result.hasErrors()) {
			this.productRepository.save(product);
			return new ModelAndView("redirect:/product/index");
		}
		else {
			return new ModelAndView("redirect:/product/index");
		}
	}
	
	
	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("product/addform");
		Product product = this.productRepository.findById(id).orElse(null);
		view.addObject("product", product);
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.productRepository.deleteById(id);
		}
		return new ModelAndView("redirect:/product/index");
	}
	
	
}
