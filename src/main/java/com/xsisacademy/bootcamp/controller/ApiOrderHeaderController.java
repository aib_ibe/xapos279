package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.OrderHeader;
import com.xsisacademy.bootcamp.repository.OrderHeaderRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")

public class ApiOrderHeaderController {
	@Autowired
	public OrderHeaderRepository orderHeaderRepository;
	
	@PostMapping("orderheader")
	public ResponseEntity<Object> saveOrderHeader(@RequestBody OrderHeader orderHeader){
		
		String timeDec = String.valueOf(System.currentTimeMillis());
		orderHeader.setReference(timeDec);
		OrderHeader orderHeaderData = this.orderHeaderRepository.save(orderHeader);
		
		if(orderHeaderData.equals(orderHeader)) {
			return new ResponseEntity<Object>("Save Success", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Save failed", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	
	@GetMapping("orderheadergetmaxid")
	public ResponseEntity<Long> getMaxOrderHeader(){
		try {
			Long orderHeader = this.orderHeaderRepository.getMaxOrderHeader();
			return new ResponseEntity<Long>(orderHeader, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("done/orderheader")
	public ResponseEntity<Object> doneProcess(@RequestBody OrderHeader orderHeader){
		Long id = orderHeader.getId();
		Optional<OrderHeader> orderheaderData = this.orderHeaderRepository.findById(id);
		
		if(orderheaderData.isPresent()) {
			orderHeader.setId(id);
			this.orderHeaderRepository.save(orderHeader);
			return new ResponseEntity<Object>("Order Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("ordered/data")
	public ResponseEntity<List<OrderHeader>> getOrderedData(){
		try {
			List<OrderHeader> orderHeader = this.orderHeaderRepository.getOrdersData();
			return new ResponseEntity<>(orderHeader, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);			
		}
	}
}
