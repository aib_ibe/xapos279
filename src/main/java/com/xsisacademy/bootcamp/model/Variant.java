package com.xsisacademy.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Variant")
public class Variant {

	@Id // id
	@GeneratedValue(strategy = GenerationType.IDENTITY) //generated as always
	@Column(name = "id") //bikin kolom id
	private Long id; // tipe data dari kolom
	
	@Column(name = "variant_code")
	private String variantCode; // referance dari file index dann addform, waktu mau nge CRUD data nya.
	
	@Column(name = "variant_name")
	private String variantName;
	
	@Column(name = "category_id")
	private Long categoryId;
	
	@Column( name = "is_active")
	private Boolean isActive;
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	// JOIN
	@ManyToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)
	public Category category;

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVariantCode() {
		return variantCode;
	}

	public void setVariantCode(String variantCode) {
		this.variantCode = variantCode;
	}

	public String getVariantName() {
		return variantName;
	}

	public void setVariantName(String variantName) {
		this.variantName = variantName;
	}
	
	
}
