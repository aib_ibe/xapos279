package com.xsisacademy.bootcamp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Variant;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	// untuk query tapi gapake query, ORM (object relational mapping)
	// JpaRepository ini berisi method-method buat ORM itu

	@Query("FROM Category WHERE LOWER(categoryName) LIKE LOWER(concat('%',?1,'%'))")
	public List<Category> searchCategory(String keyword);
	
	@Query("FROM Category ORDER BY categoryCode")
	public List<Category>findByCategory();
	
	@Modifying
	@Query(value="UPDATE Category SET is_active = false WHERE id = ?1", nativeQuery = true)
	@Transactional
	public void deleteCategoryById(Long id);
	
	
	@Query("FROM Variant")
	public List<Variant>findByVariant();
	
	@Modifying
	@Query(value="UPDATE Variant SET is_active = false WHERE category_id = ?1", nativeQuery = true)
	@Transactional
	public void deleteVariantByCategory(Long id);
		
	
	public Page<Category>findByIsActiveTrueOrderByCategoryCode(Pageable pageable);
}
