package com.xsisacademy.bootcamp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long>{
	@Query("FROM Variant ORDER BY variantCode")
	public List<Variant>findVariantOrdered();
	
	@Modifying
	@Query(value="UPDATE Variant SET is_active = false WHERE id = ?1", nativeQuery = true)
	@Transactional
	public void deleteVariantById(Long id);
		
}
