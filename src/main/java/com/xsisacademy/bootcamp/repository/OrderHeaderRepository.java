package com.xsisacademy.bootcamp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.OrderHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Long>{
	
	@Query("SELECT MAX(id) as maxid FROM OrderHeader")
	public Long getMaxOrderHeader();
	
	@Query("FROM OrderHeader WHERE amount != '0' ORDER BY reference")
	public List<OrderHeader> getOrdersData();
}
