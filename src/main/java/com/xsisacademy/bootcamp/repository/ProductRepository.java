package com.xsisacademy.bootcamp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;

public interface ProductRepository extends JpaRepository<Product, Long>{
	
	@Query("FROM Variant WHERE category_id = ?1")
	public List<Variant> findVariantByCategory(Long id);
	
	@Modifying
	@Query(value="UPDATE Product SET is_active = false WHERE id = ?1", nativeQuery = true)
	@Transactional
	public void deleteProductById(Long id);
	
	@Query("FROM Product ORDER BY productCode")
	public List<Product>findProductOrdered();
}
